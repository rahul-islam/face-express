# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 17:34:59 2016

@author: Rahul
"""

import gabor
import adaboost
import numpy as np
from scipy.misc import imresize
from skimage import io
from matplotlib import pyplot as plt  
from sklearn.datasets import load_digits
from sklearn.ensemble import AdaBoostClassifier
from sklearn.svm import LinearSVC
iris = load_digits()
#clf = AdaBoostClassifier()
#clf.fit(iris.data,iris.target)
features = np.load('./data/features/features.npy')
#features[:,1:] = iris.data
#features[:,0] = iris.target

#indz,model = adaboost.adaboost(features,k)
indz = adaboost.adaboostIndices(features)
# add 1 to to move index one step forward to acomodate label
indz = indz+1
# append 0 to get labels
indz = np.append(indz,[0])
#sort all indices
indz = np.sort(indz)

np.save('./data/ada/ada_indices_with.npy',indz)