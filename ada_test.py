# -*- coding: utf-8 -*-
"""
Created on Fri Feb 12 11:20:17 2016

@author: Rahul
"""

import adaboost
import numpy as np
from sklearn import cross_validation


features = np.load('./data/features/features.npy')
X_train, X_test, y_train, y_test = cross_validation.train_test_split(features[:,1:], features[:,0], test_size=0.4, random_state=0)
x = np.zeros(shape=(196,127009))
x[:,0] = y_train
x[:,1:] = X_train
features = x

indz, model = adaboost.adaboost(features,1)