# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 13:31:53 2016

@author: Rahul
"""

import gabor
import numpy as np
from skimage import io
from scipy import signal
from scipy.misc import imresize

g = gabor.gabor_fn(bandwidth=1,theta=0,Lambda=32,psi=0,gamma=0.5)
image = io.imread('test.png')
#image = imresize(image,(42,42))
#k = np.sqrt(2)
#_lambda  = [2, 2*k, 4, 4*k, 8, 8*k, 16, 16*k, 32]
#_theta   = 0;
#_psi     = [0, np.pi/2];
#_gamma   = 0.5;
#_bw      = 1;
#_N       = 8;
#_freq = 9;
#_outMag = np.zeros(shape=(np.size(image,0),np.size(image,1), _N*_freq),dtype=np.complex)
#
#for _index, _fq in enumerate(_lambda):
#    print(_index)
#    for n in range(_N):
#        #print("o")
#        
#        gb = gabor.gabor_fn(_bw,_theta,_lambda[_index],_psi[0],_gamma)+ 1j * gabor.gabor_fn(_bw,_theta,_lambda[_index],_psi[1],_gamma);
#        _outMag[:,:,_index*8+n] = signal.convolve2d(image,gb,mode='same')
#    
#    feature_vector = np.reshape(_outMag,(1,np.size(image,0)*np.size(image,1)*_freq*_N))
#    feature_vector = np.sqrt(np.real(feature_vector)*np.real(feature_vector)+np.imag(feature_vector)*np.imag(feature_vector));
io.imshow(gabor.imggaborfilter(g,image))


        