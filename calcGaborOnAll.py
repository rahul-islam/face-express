# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 21:37:32 2016

Use this to cal gabor on all imagesin data/croped_ck+/ and save
to gabor_feature so it can be used to for feature selection using adaboost 
or random forest

@author: Rahul
"""
import gabor
import numpy as np
from scipy.misc import imresize
from skimage import io
from skimage.color import rgb2gray
from matplotlib import pyplot as plt  
import os,sys


ck_path = './data/croped_ck+/42x42/'
emo = np.load('./data/emo/emo_py.npy')
im42_list =  os.listdir(ck_path)
gabor_feature = np.zeros(shape=(327,127009))
for i,val in enumerate(im42_list[:]):
    print('procesing\t::\t'+val)
    image = io.imread(ck_path+val)
    image = rgb2gray(image)
    image = np.double(image)
    if val == emo[i,0]+'.png' :   
        gbr = gabor.gabor_f(image)
        #gbr = np.append(emo[1,1],gbr)
        #gbr = np.reshape(gbr,(1,127009))
        gabor_feature[i,0] = emo[i,1]
        gabor_feature[i:,1:] = gbr