# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 17:58:59 2016

@author: Rahul
"""

import numpy as np
from skimage import io, color
from scipy.misc import imresize
import gabor

indz = np.load('./data/ada/indz.npy')
svmStruct = np.load('./data/svm.npy')

indz = indz.astype(int)

image = io.imread('./test.png')
image = np.double(color.rgb2gray(image))

image = imresize(image,(42,42))

image_f = gabor.gabor_f(image)

image_f = image_f[:,indz[1:]]
label_perm = ['Angry','Neutral','Disgust','Fear','Happy','Sadness','Surprise']
for i in range(7):
    rs = svmStruct[i].predict(image_f)
    print(str(i+1)+"\t"+label_perm[i]+str(rs))

