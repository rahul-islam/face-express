# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 21:58:43 2016

@author: Rahul
"""
import numpy as np
from scipy.misc import imresize
from skimage import io
from matplotlib import pyplot as plt  
from sklearn.ensemble import AdaBoostClassifier
from scipy import ndimage as ndi

__all__ = ['adaboost','adaboostIndices']

"""
Helper Function adaboostIndices()

"""
def adaboost(features,k):
    print("Adaboost Learning for Class\t::\t"+str(k))
    #feature which belons to class k
    featurek = features[features[:,0]==k,:]
    print('started')
    #feature which do not belog to class k
    featurenotk = features[features[:,0]!=k,:]
    print('started')
    featurek[:,0]=1 
    print('started')
    featurenotk[:,0]=2
    print('started')
    new_features=np.append(featurek, featurenotk,axis=0)
    
    labels = new_features[:,0]
    data = new_features[:,1:]
    print('started')
    model = AdaBoostClassifier(n_estimators=300)
    model.fit(data,labels)
    g = model.feature_importances_
    idx = []
    #index  = np.array([1])
    for i,val in enumerate(g):
        if val > 0:
            idx = np.append(idx,i)
    return idx, model

"""
return selected features indices and save to directory (./data/ada/)

"""
def adaboostIndices(features):
    target = np.unique(features[:,0])
    target = target.astype(int)
    ind = []
    for i in target:
        #ind = adaboost.adaboost(features,k=i)    
        #print(target[i])
        inex,model= adaboost(features,k=i)
        ind = np.append(ind,inex)
    #indices = indices.astype(int)
    
    #indices = np.unique(ind)
    indices = ind
    
    np.save('./data/ada/ada_indices.npy',indices)
    return indices
    
    