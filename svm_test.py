# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 06:47:27 2016

@author: MakeInIndia
"""

# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 21:37:32 2016

@author: Rahul
"""
import gabor
import adaboost
import numpy as np
from scipy.misc import imresize
from skimage import io
from matplotlib import pyplot as plt  
from sklearn.datasets import load_digits
from sklearn import cross_validation
from sklearn.svm import LinearSVC
iris = load_digits()
#clf = AdaBoostClassifier()
#clf.fit(iris.data,iris.target)
indz = np.load('./data/ada/ada_indices_with.npy')
indz = indz.astype(int)
features = np.load('./data/features/features.npy')
X_train, X_test, y_train, y_test = cross_validation.train_test_split(features[:,1:], features[:,0], test_size=0.4, random_state=0)
#features = np.zeros(shape=(1797,65))
#features[:,1:] = iris.data
#features[:,0] = iris.target

#indz = adaboost.adaboostIndices(features)
## add 1 to to move index one step forward to acomodate label
#indz = indz+1
## append 0 to get labels
#indz = np.append(indz,[0])
##sort all indices
#indz = np.sort(indz)

#indz = indz.astype(int)

#test = features[301:,:]
#features = features[0:300,:]

""" """
x = np.zeros(shape=(196,127009))
x[:,0] = y_train
x[:,1:] = X_train
features = x
#features = features[:,indz]
x = np.zeros(shape=(131,127009))
x[:,0] = y_test
x[:,1:] = X_test
test = x
#test = test[:,indz]
data = features[:,1:]
labels = np.unique(features[:,0])
labels = labels.astype(int)


"""  """
svmStruct = []
score = []
for i in labels:
    print(i)
    
    featurek = features[features[:,0]==i,:]
    
    #feature which do not belog to class k
    featurenotk = features[features[:,0]!=i,:]
    
    featurek[:,0]=11 
    featurenotk[:,0]=22
    
    new_features=np.append(featurek, featurenotk,axis=0)
    
    _labels = new_features[:,0]
    _labels = _labels.astype(int)
    data = new_features[:,1:]
    clf = LinearSVC()
    clf.fit(data,_labels)
    #validation
    testk = test[test[:,0]==i,:]
    #feature which do not belog to class k
    testnotk = test[test[:,0]!=i,:]
    
    testk[:,0]=11 
    testnotk[:,0]=22
    new_test=np.append(testk, testnotk,axis=0)
    score.append(clf.score(new_test[:,1:],new_test[:,0]))
    svmStruct.append(clf)
#print("D")
#featurek = features[features[:,0]==1,:]
#
##feature which do not belog to class k
#featurenotk = features[features[:,0]!=1,:]
#
#featurek[:,0]=11 
#featurenotk[:,0]=22
#
#new_features=np.append(featurek, featurenotk,axis=0)
#
#_labels = new_features[:,0]
#_labels = _labels.astype(int)
#data = new_features[:,1:]
#clf = LinearSVC()
#print("fit")
#clf.fit(data,_labels)
##validation
#testk = test[test[:,0]==1,:]
##feature which do not belog to class k
#testnotk = test[test[:,0]!=1,:]
#
#testk[:,0]=11 
#testnotk[:,0]=22
#new_test=np.append(testk, testnotk,axis=0)
#clf.score(new_test[:,1:],new_test[:,0])
