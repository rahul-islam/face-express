# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 21:37:32 2016

@author: Rahul
"""
import gabor
import adaboost
import numpy as np
from scipy.misc import imresize
from skimage import io
from matplotlib import pyplot as plt  
from sklearn.datasets import load_digits
from sklearn.ensemble import AdaBoostClassifier
from sklearn.svm import LinearSVC
iris = load_digits()
#clf = AdaBoostClassifier()
#clf.fit(iris.data,iris.target)
features = np.load('./data/features/features.npy')
#features[:,1:] = iris.data
#features[:,0] = iris.target

indz = adaboost.adaboostIndices(features)
# add 1 to to move index one step forward to acomodate label
indz = indz+1
# append 0 to get labels
indz = np.append(indz,[0])
#sort all indices
indz = np.sort(indz)
features = features[:,indz]
f = features[1701:,:]
features = features[0:1700,:]

data = features
labels = features[:,0]

nClasses = np.size(np.unique(labels))
#class_data=[]
#for i in range(nClasses):
#    class_data.append(data[labels==i,:])

Classes = []
svmStruct = []
for i in range(nClasses):
    featurek = features[features[:,0]==i,:]
    
    #feature which do not belog to class k
    featurenotk = features[features[:,0]!=i,:]
    
    featurek[:,0]=11 
    featurenotk[:,0]=22
    
    new_features=np.append(featurek, featurenotk,axis=0)
    
    labels = new_features[:,0]
    data = new_features[:,1:]
    clf = LinearSVC()
    clf.fit(data,labels)
    svmStruct.append(clf)
    