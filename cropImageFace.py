# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 21:37:32 2016

This module is used to crop faces of the subject and save them.
it also generate emotion lable mat fie see in data folder.

@author: Rahul
"""
import gabor
import numpy as np
from scipy.misc import imresize
from skimage import io
from skimage.color import rgb2gray
from matplotlib import pyplot as plt  
import os,sys
import dlib
#image = io.imread('test.png')
#g_r,g_i = gabor_filter(image,frequency=.2,theta=np.pi/4)
#plt.figure()
#io.imshow(np.sqrt(g_r*g_r+g_i*g_i))
#io.show()
detector = dlib.get_frontal_face_detector()
#image = imresize(image,(42,42))
ck_path = './data/ck+/cohn-kanade-images/'
emotion_lbl_path = './data/Emotion_labels/'
emo_lable = np.array([(0,0)])
emo_list =  os.listdir(emotion_lbl_path)
print(emo_list[0])
for subject in emo_list:
    sub_img_list = os.listdir(emotion_lbl_path+subject+'/')
    for img_dir in sub_img_list:
        im_emo = os.listdir(emotion_lbl_path+subject+'/'+img_dir+'/')
        for emo in im_emo:
            el=np.genfromtxt(emotion_lbl_path+subject+'/'+img_dir+'/'+emo)
            emo_lable = np.append(emo_lable,[(emo.replace('_emotion.txt',''),el)],axis=0)
            emo=emo.replace('_emotion.txt','')
            image = io.imread(ck_path+subject+'/'+img_dir+'/'+emo+'.png')
            #image = rgb2gray(image)
            #io.imshow(image)
            dets = detector(image, 1)
            image = image[dets[0].top():dets[0].bottom(),dets[0].left():dets[0].right()]
            im42 = imresize(image,(42,42))
            im192 = imresize(image,(192,192))
            io.imsave("./data/croped_ck+/42x42/"+emo+".png",im42)
            io.imsave("./data/croped_ck+/192x192/"+emo+".png",im192)  
            print("Processing\t::\t"+emo)