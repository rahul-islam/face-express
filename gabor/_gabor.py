# -*- coding: utf-8 -*-
"""
Created on Sun Feb  7 21:58:43 2016

@author: Rahul
"""
import numpy as np
from skimage.filters import gabor_filter as gabor_fil
from scipy import ndimage as ndi
from scipy import signal

__all__ = ['gabor_kernel', 'gabor_filter', 'gabor_extract','gabor_fn','gabor_f','imggaborfilter']


def _sigma_prefactor(bandwidth):
    b = bandwidth
    # See http://www.cs.rug.nl/~imaging/simplecell.html
    return 1.0 / np.pi * np.sqrt(np.log(2) / 2.0) * \
        (2.0 ** b + 1) / (2.0 ** b - 1)

def gabor_fn(bandwidth,theta,Lambda,psi,gamma):
    sigma = Lambda/np.pi*np.sqrt(np.log(2)/2)*(2**bandwidth+1)/(2**bandwidth-1)
    sigma_x = sigma;
    sigma_y = sigma/gamma;
    
    # Bounding box
    nstds = 3;
    xmax = max(abs(nstds*sigma_x*np.cos(theta)),abs(nstds*sigma_y*np.sin(theta)));
    xmax = np.ceil(max(1,xmax));
    ymax = max(abs(nstds*sigma_x*np.sin(theta)),abs(nstds*sigma_y*np.cos(theta)));
    ymax = np.ceil(max(1,ymax));
    xmin = -xmax; ymin = -ymax;
    (x,y) = np.meshgrid(np.arange(xmin,xmax+1),np.arange(ymin,ymax+1 ));
    (y,x) = np.meshgrid(np.arange(ymin,ymax+1),np.arange(xmin,xmax+1 ));
    
    # Rotation 
    x_theta=x*np.cos(theta)+y*np.sin(theta);
    y_theta=-x*np.sin(theta)+y*np.cos(theta);
    
    gb= np.exp(-.5*(x_theta**2/sigma_x**2+y_theta**2/sigma_y**2))*np.cos(2*np.pi/Lambda*x_theta+psi);
    return gb
    
def gabor_f(image):
    k = np.sqrt(2)
    _lambda  = [2, 2*k, 4, 4*k, 8, 8*k, 16, 16*k, 32]
    _theta   = 0;
    _psi     = [0, np.pi/2];
    _gamma   = 0.5;
    _bw      = 1;
    _N       = 8;
    _freq = 9;
    _outMag = np.zeros(shape=(np.size(image,0),np.size(image,1), _N*_freq),dtype=np.complex)
    
    for _index, _fq in enumerate(_lambda):
        print(_index)
        for n in range(_N):
            #print("o")
            
            gb = gabor_fn(_bw,_theta,_lambda[_index],_psi[0],_gamma)+ 1j * gabor_fn(_bw,_theta,_lambda[_index],_psi[1],_gamma);
            _outMag[:,:,_index*8+n] = imggaborfilter(gb, image )
        
        feature_vector = np.reshape(_outMag,(1,np.size(image,0)*np.size(image,1)*_freq*_N))
        feature_vector = np.sqrt(np.real(feature_vector)*np.real(feature_vector)+np.imag(feature_vector)*np.imag(feature_vector));

    return feature_vector

def imggaborfilter(gabor, image ):
    gabor_img = signal.convolve2d(image,gabor,mode='same')
    return gabor_img
    
    
def gabor_kernel(frequency, theta=0, bandwidth=1, sigma_x=None, sigma_y=None,
                 n_stds=3, offset=0):
    """Return complex 2D Gabor filter kernel.
    Gabor kernel is a Gaussian kernel modulated by a complex harmonic function.
    Harmonic function consists of an imaginary sine function and a real
    cosine function. Spatial frequency is inversely proportional to the
    wavelength of the harmonic and to the standard deviation of a Gaussian
    kernel. The bandwidth is also inversely proportional to the standard
    deviation.
    Parameters
    ----------
    frequency : float
        Spatial frequency of the harmonic function. Specified in pixels.
    theta : float, optional
        Orientation in radians. If 0, the harmonic is in the x-direction.
    bandwidth : float, optional
        The bandwidth captured by the filter. For fixed bandwidth, `sigma_x`
        and `sigma_y` will decrease with increasing frequency. This value is
        ignored if `sigma_x` and `sigma_y` are set by the user.
    sigma_x, sigma_y : float, optional
        Standard deviation in x- and y-directions. These directions apply to
        the kernel *before* rotation. If `theta = pi/2`, then the kernel is
        rotated 90 degrees so that `sigma_x` controls the *vertical* direction.
    n_stds : scalar, optional
        The linear size of the kernel is n_stds (3 by default) standard
        deviations
    offset : float, optional
        Phase offset of harmonic function in radians.
    Returns
    -------
    g : complex array
        Complex filter kernel.
    References
    ----------
    .. [1] http://en.wikipedia.org/wiki/Gabor_filter
    .. [2] http://mplab.ucsd.edu/tutorials/gabor.pdf
    Examples
    --------
    >>> from skimage.filters import gabor_kernel
    >>> from skimage import io
    >>> from matplotlib import pyplot as plt  # doctest: +SKIP
    >>> gk = gabor_kernel(frequency=0.2)
    >>> plt.figure()        # doctest: +SKIP
    >>> io.imshow(gk.real)  # doctest: +SKIP
    >>> io.show()           # doctest: +SKIP
    >>> # more ripples (equivalent to increasing the size of the
    >>> # Gaussian spread)
    >>> gk = gabor_kernel(frequency=0.2, bandwidth=0.1)
    >>> plt.figure()        # doctest: +SKIP
    >>> io.imshow(gk.real)  # doctest: +SKIP
    >>> io.show()           # doctest: +SKIP
    """
    if sigma_x is None:
        sigma_x = _sigma_prefactor(bandwidth) / frequency
    if sigma_y is None:
        sigma_y = _sigma_prefactor(bandwidth) / frequency

    x0 = np.ceil(max(np.abs(n_stds * sigma_x * np.cos(theta)),
                     np.abs(n_stds * sigma_y * np.sin(theta)), 1))
    y0 = np.ceil(max(np.abs(n_stds * sigma_y * np.cos(theta)),
                     np.abs(n_stds * sigma_x * np.sin(theta)), 1))
    y, x = np.mgrid[-y0:y0 + 1, -x0:x0 + 1]

    rotx = x * np.cos(theta) + y * np.sin(theta)
    roty = -x * np.sin(theta) + y * np.cos(theta)

    g = np.zeros(y.shape, dtype=np.complex)
    g[:] = np.exp(-0.5 * (rotx ** 2 / sigma_x ** 2 + roty ** 2 / sigma_y ** 2))
    g /= 2 * np.pi * sigma_x * sigma_y
    g *= np.exp(1j * (2 * np.pi * frequency * rotx + offset))

    return g

def gabor_filter(image, frequency, theta=0, bandwidth=1, sigma_x=None,
          sigma_y=None, n_stds=3, offset=0, mode='reflect', cval=0):
    """Return real and imaginary responses to Gabor filter.
    The real and imaginary parts of the Gabor filter kernel are applied to the
    image and the response is returned as a pair of arrays.
    Gabor filter is a linear filter with a Gaussian kernel which is modulated
    by a sinusoidal plane wave. Frequency and orientation representations of
    the Gabor filter are similar to those of the human visual system.
    Gabor filter banks are commonly used in computer vision and image
    processing. They are especially suitable for edge detection and texture
    classification.
    Parameters
    ----------
    image : 2-D array
        Input image.
    frequency : float
        Spatial frequency of the harmonic function. Specified in pixels.
    theta : float, optional
        Orientation in radians. If 0, the harmonic is in the x-direction.
    bandwidth : float, optional
        The bandwidth captured by the filter. For fixed bandwidth, `sigma_x`
        and `sigma_y` will decrease with increasing frequency. This value is
        ignored if `sigma_x` and `sigma_y` are set by the user.
    sigma_x, sigma_y : float, optional
        Standard deviation in x- and y-directions. These directions apply to
        the kernel *before* rotation. If `theta = pi/2`, then the kernel is
        rotated 90 degrees so that `sigma_x` controls the *vertical* direction.
    n_stds : scalar, optional
        The linear size of the kernel is n_stds (3 by default) standard
        deviations.
    offset : float, optional
        Phase offset of harmonic function in radians.
    mode : {'constant', 'nearest', 'reflect', 'mirror', 'wrap'}, optional
        Mode used to convolve image with a kernel, passed to `ndi.convolve`
    cval : scalar, optional
        Value to fill past edges of input if `mode` of convolution is
        'constant'. The parameter is passed to `ndi.convolve`.
    Returns
    -------
    real, imag : arrays
        Filtered images using the real and imaginary parts of the Gabor filter
        kernel. Images are of the same dimensions as the input one.
    References
    ----------
    .. [1] http://en.wikipedia.org/wiki/Gabor_filter
    .. [2] http://mplab.ucsd.edu/tutorials/gabor.pdf
    Examples
    --------
    >>> from skimage.filters import gabor
    >>> from skimage import data, io
    >>> from matplotlib import pyplot as plt  # doctest: +SKIP
    >>> image = data.coins()
    >>> # detecting edges in a coin image
    >>> filt_real, filt_imag = gabor(image, frequency=0.6)
    >>> plt.figure()            # doctest: +SKIP
    >>> io.imshow(filt_real)    # doctest: +SKIP
    >>> io.show()               # doctest: +SKIP
    >>> # less sensitivity to finer details with the lower frequency kernel
    >>> filt_real, filt_imag = gabor(image, frequency=0.1)
    >>> plt.figure()            # doctest: +SKIP
    >>> io.imshow(filt_real)    # doctest: +SKIP
    >>> io.show()               # doctest: +SKIP
    """

    g = gabor_kernel(frequency, theta, bandwidth, sigma_x, sigma_y, n_stds,
                     offset)

    filtered_real = ndi.convolve(image, np.real(g), mode=mode, cval=cval)
    filtered_imag = ndi.convolve(image, np.imag(g), mode=mode, cval=cval)

    return filtered_real, filtered_imag

def gabor_extract(image):
    _k = np.sqrt(2)
    #_lambda  = [0.1, .02*_k, .04, .04*_k, .08, .08*_k, .16, .16*_k, .32,]
    _lambda  = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
    _theta   = [0, np.pi/4, np.pi/2, 3*np.pi/4, np.pi, 5*np.pi/4, 3*np.pi/2,7*np.pi/4]
    _psi     = [0, np.pi/2]
    _N        = 8
    _freq     = 9

    _outMag = np.zeros(shape=(np.size(image,0),np.size(image,1), _N*_freq),dtype=np.double)

    for _index, _fq in enumerate(_lambda):
        #print(_index)
        for n in range(_N):
            #print("o")
            gfea_real, gfea_imag = gabor_filter(image,frequency=_fq,theta=_theta[n])
            #print(_index*8+n)
            gfea_real = gfea_real*gfea_real
            #print("raea")
            gfea_imag = gfea_imag*gfea_imag
            #print("imag")
            gfea = gfea_real+gfea_imag
            gfea = np.sqrt(gfea)
            #print("yo")
            _outMag[:,:,_index*8+n] = gfea
            #_theta = _theta + 2*np.pi/(_N+1);
            #print(_theta)

    feature_vec  = np.reshape(_outMag,(1,np.size(image,0)*np.size(image,1)*_freq*_N))

    return feature_vec
